import os
import stat
import time


MAXTIME = 60 * 60 * 24


def get_filestore_root():
    try:
        from django.conf import settings
    except ImportError:
        settings = object()

    if hasattr(settings, "FILESTORE_ROOT"):
        return settings.FILESTORE_ROOT

    if "FILESTORE_ROOT" in os.environ:
        return os.environ["FILESTORE_ROOT"]

    if hasattr(settings, "MEDIA_ROOT"):
        return settings.MEDIA_ROOT

    import tempfile
    return tempfile.mkdtemp()


class Store(object):
    def __init__(self, store=None):
        self.path = os.path.join(get_filestore_root(), 'filestore', store)

    def filepath(self, key):
        fullpath = os.path.join(self.path, key[:2], key)
        os.makedirs(os.path.dirname(fullpath), exist_ok=True)
        return fullpath

    def set(self, key, data):
        path = self.filepath(key)
        with open(path, 'wb') as f:
            f.write(data)

    def get(self, key, since=None, maxtime=MAXTIME):
        if since:
            secs = time.mktime(since.timetuple())
        else:
            secs = time.time() - maxtime

        path = self.filepath(key)
        try:
            fstat = os.stat(path)
        except FileNotFoundError:
            return None

        if fstat[stat.ST_MTIME] < secs:
            return None

        with open(path, 'rb') as f:
            content = f.read()
        return content
