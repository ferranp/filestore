#!/usr/bin/env python
import os
from distutils.core import setup

__author__ = u'Ferran Pegueroles'
__copyright__ = u'Copyright 2017, Ferran Pegueroles'
__credits__ = [u'Ferran Pegueroles']


__license__ = 'BSD'
__version__ = '0.1.7'
__email__ = 'ferran@pegueroles.com'


long_description = ""

setup(
    name='filestore',
    version=__version__,
    url='http://bitbucket.org/ferranp/qc_common',
    author=__author__,
    author_email=__email__,
    license='GPL',
    packages=['filestore',
              ],
    description='File cache',
    long_description=long_description,
    classifiers=['Development Status :: 4 - Beta',
                 'Environment :: Web Environment',
                 'Framework :: Django',
                 'Intended Audience :: Developers',
                 'License :: OSI Approved :: BSD License',
                 'Topic :: Internet :: WWW/HTTP :: Dynamic Content'],
    install_requires=['python-ldap'],
    )
